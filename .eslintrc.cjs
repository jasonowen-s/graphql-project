module.exports = {
    extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended'],
    parser: '@typescript-eslint/parser',
    plugins: ['@typescript-eslint'],
    root: true,
    env: { node: true },
    rules: {
        'comma-dangle': ['error', 'never'],
        'no-trailing-spaces': ['error'],
        quotes: ['error', 'single'],
        'object-curly-spacing': ['error', 'always'],
        semi: ['error', 'always'],
        'no-multiple-empty-lines': ['error', { 'max': 1, 'maxEOF': 0 }],
        'indent': ['error', 4]
    }
};
