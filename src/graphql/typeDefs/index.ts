import companyTypeDefs from './company';
import userTypeDefs from './user';

const commonTypeDefs = `
  type CreatedDocument {
    id: String
  }
  type Status {
    success: Boolean
  }
  type UserAndCompany {
    name: String
    email: String
    yearOfCreation: Int
    users: [User]
    companies: [Company]
  }
  input UpdateEntityInput {
    name: String
    email: String
    yearOfCreation: Int
  }
  type Query {
    getMyInfo: UserAndCompany
  }
  type Mutation {
    updateMyInfo(input: UpdateEntityInput!): Status
  }
`;

export default [commonTypeDefs, userTypeDefs, companyTypeDefs];
