const typeDefs = `
  type Company {
    name: String
    yearOfCreation: Int
    users: [User]
  }

  input CreateCompanyInput {
    name: String!
    yearOfCreation: Int!
  }

  type Mutation {
    createCompany(input: CreateCompanyInput!): CreatedDocument
    addUser(input: String!): Status
    deleteUser(input: String): Status
    deleteAllUsers: Status
  }
`;

export default typeDefs;
