const typeDefs = `
    type User {
      _id: String
      name: String
      email: String
      companies: [Company]
    }

    input CreateUserInput {
      name: String!
      email: String!
    }

    type Mutation {
      createUser(input: CreateUserInput!): CreatedDocument
      addCompany(input: String!): Status
      deleteMyCompany(input: String!): Status
    }
  `;

export default typeDefs;
