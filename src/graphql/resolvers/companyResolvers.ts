import pick from 'lodash/pick';

import { authenticateCompany } from '../../middlewares';
import { UserRepository } from '../../repositories';
import { BaseCompany, UpdateCompanyInput, User } from '../../types';
import { ServerContext } from '../../types/graphql';

const throwIfUserNotExists = async (userRepository: UserRepository, userId: string) =>{
    const user = await userRepository.findOne(userId);
    if (!user) {
        throw new Error('User not found');
    }
};

const checkIfUserExists = (userId: string, users: User[]) => {
    const alreadyExists = (user: User) => user._id.toString() === userId;
    return users.some(alreadyExists);
};

const createCompany = async (_: object, args: {input: BaseCompany}, ctx: ServerContext) => {
    const { input: company } = args;
    const { insertedId } = await ctx.companyRepository.createOne(company);
    return { id: insertedId };
};

const addUser = async (_: object, args: {input: string}, ctx: ServerContext) => {
    const { companyRepository, userRepository } = ctx;
    const { input: userId } = args;
    const { _id: companyId, users } = await authenticateCompany(ctx);

    // Verification
    if(checkIfUserExists(userId, users)) {
        throw new Error('User already exists');
    }
    await throwIfUserNotExists(userRepository, userId);

    // Changes
    await companyRepository.addUser(companyId, userId);
    await userRepository.addCompany(userId, companyId);

    return {
        success: true
    };
};

const deleteUser = async (_: object, args: {input: string}, ctx: ServerContext) => {
    const { companyRepository, userRepository } = ctx;
    const { input: userId } = args;
    const { _id: companyId, users } = await authenticateCompany(ctx);

    // Verification
    if(!checkIfUserExists(userId, users)) {
        throw new Error('Company not registered to user');
    }
    await throwIfUserNotExists(userRepository, userId);

    // Changes
    await userRepository.removeCompany(userId, companyId);
    await companyRepository.removeUser(companyId, userId);

    return {
        success: true
    };
};

const deleteAllUsers = async (_: object, args: object, ctx: ServerContext) => {
    const { companyRepository, userRepository } = ctx;
    const { _id: companyId, users } = await authenticateCompany(ctx);

    // Changes
    const userIds = users.map(({ _id }) => _id);
    await userRepository.removeCompanyFromUsers(userIds, companyId);
    await companyRepository.removeAllUsers(companyId);

    return {
        success: true
    };
};

const updateCompanyInfo =  async (input: UpdateCompanyInput, ctx: ServerContext) => {
    const { companyId, companyRepository } = ctx;
    const { name, yearOfCreation } = input;
    if(!name && !yearOfCreation) {
        throw new Error('Update company payload invalid');
    }
    const payload = pick(input, ['name', 'yearOfCreation']);
    await companyRepository.updateOne(companyId, payload);
};

const mutations = {
    createCompany,
    addUser,
    deleteUser,
    deleteAllUsers
};

export {
    mutations,
    updateCompanyInfo
};
