import pick from 'lodash/pick';

import { authenticateUser } from '../../middlewares';
import { CompanyRepository } from '../../repositories';
import { BaseUser, Company, UpdateUserInput } from '../../types';
import { ServerContext } from '../../types/graphql';

const throwIfCompanyNotExists = async (companyRepository: CompanyRepository, companyId: string) =>{
    const company = await companyRepository.findOne(companyId);
    if (!company) {
        throw new Error('Company not found');
    }
};

const checkIfCompanyRegistered = (companyId: string, companies: Company[]) => {
    const alreadyExists = (company: Company) => company._id.toString() === companyId;
    return companies.some(alreadyExists);
};

const createUser = async (_: object, args: {input: BaseUser}, ctx: ServerContext) => {
    const { input: user } = args;
    const { insertedId } = await ctx.userRepository.createOne(user);
    return { id: insertedId };
};

const addCompany = async (_: object, args: {input: string}, ctx: ServerContext) => {
    const { companyRepository, userRepository } = ctx;
    const { input: companyId } = args;
    const { _id: userId, companies } = await authenticateUser(ctx);

    // Verification
    if(checkIfCompanyRegistered(companyId, companies)) {
        throw new Error('Company already registered');
    }
    await throwIfCompanyNotExists(companyRepository, companyId);

    // Changes
    await userRepository.addCompany(userId, companyId);
    await companyRepository.addUser(companyId, userId);

    return {
        success: true
    };
};

const deleteMyCompany = async (_: object, args: {input: string}, ctx: ServerContext) => {
    const { companyRepository, userRepository } = ctx;
    const { input: companyId } = args;
    const { _id: userId, companies } = await authenticateUser(ctx);

    // Verification
    if(!checkIfCompanyRegistered(companyId, companies)) {
        throw new Error('Company not registered to user');
    }
    await throwIfCompanyNotExists(companyRepository, companyId);

    // Changes
    await userRepository.removeCompany(userId, companyId);
    await companyRepository.removeUser(companyId, userId);

    return {
        success: true
    };
};

const updateUserInfo =  async (input: UpdateUserInput, ctx: ServerContext) => {
    const { userId, userRepository } = ctx;
    const  { name, email } = input;
    if(!email && !name) {
        throw new Error('Update user payload invalid');
    }
    const payload = pick(input, ['name', 'email']);
    await userRepository.updateOne(userId, payload);
};

const mutations = {
    createUser,
    addCompany,
    deleteMyCompany
};

export {
    mutations,
    updateUserInfo
};
