import { authenticateEntity } from '../../middlewares';
import { UpdateEntityInput } from '../../types';
import { ServerContext } from '../../types/graphql';

import { mutations as companyMutations, updateCompanyInfo } from './companyResolvers';
import { mutations as userMutations, updateUserInfo } from './userResolvers';

// Shared Resolvers since the entry points for user and company is the same
const getMyInfo = async (_: object, __: object, ctx: ServerContext) => {
    const { entity } = await authenticateEntity(ctx);
    return entity;
};

const updateMyInfo = async (_: object, args: {input: UpdateEntityInput}, ctx: ServerContext) => {
    const { role } = await authenticateEntity(ctx);
    const resolvers = {
        company: updateCompanyInfo,
        user: updateUserInfo
    };
    await resolvers[role](args.input, ctx);

    return {
        success: true
    };
};

const resolvers = {
    Query: {
        getMyInfo: getMyInfo
    },
    Mutation: {
        ...companyMutations,
        ...userMutations,
        updateMyInfo
    }
};

export default resolvers;
