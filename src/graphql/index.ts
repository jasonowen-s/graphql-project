import resolvers from './resolvers';
import commonTypeDefs from './typeDefs';

export {
    commonTypeDefs as typeDefs,
    resolvers
};
