import dotenv from 'dotenv';

dotenv.config();

const config = {
    env: process.env.NODE_ENV,
    port: process.env.PORT,
    db: {
        connectionString: process.env.DB_CONN_STRING,
        dbName: process.env.DB_NAME
    }
};

export default config;
