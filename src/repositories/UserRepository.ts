import { ObjectId, type Collection } from 'mongodb';

import { BaseUser, UpdateUserInput } from '../types';

class UserRepository {
    db: Collection<BaseUser>;
    constructor({ db }) {
        this.db = db;
    }

    async findOne(userId: string) {
        const companyLookup = {
            from: 'company',
            localField: 'companies',
            foreignField: '_id',
            as: 'companies'
        };
        const findById = [
            { $match: { _id: new ObjectId(userId) } },
            { $lookup: companyLookup }
        ];
        const [user] = await this.db.aggregate(findById).toArray();
        return user;
    }

    async createOne(user: BaseUser) {
        const now = new Date();
        const defaultAttributes = {
            companies: [],
            createdAt: now,
            updatedAt: now
        };
        Object.assign(user, defaultAttributes);
        return this.db.insertOne(user);
    }

    async addCompany(userId: string, companyId: string) {
        return this.db.updateOne(
            { _id: new ObjectId(userId) },
            { $push: { companies: new ObjectId(companyId) } }
        );
    }

    async updateOne(userId: string, user: UpdateUserInput) {
        const now = new Date();
        const updateAttribute = {
            updatedAt: now
        };
        Object.assign(user, updateAttribute);
        return this.db.updateOne(
            { _id: new ObjectId(userId) },
            { $set: user }
        );
    }

    async removeCompany(userId: string, companyId: string) {
        return this.db.updateOne(
            { _id: new ObjectId(userId) },
            { $pull: { companies: new ObjectId(companyId) } }
        );
    }

    async removeCompanyFromUsers(userIds: [ObjectId], companyId: string) {
        return this.db.updateMany(
            { _id: { $in: userIds } },
            { $pull: { companies: new ObjectId(companyId) } }
        );
    }
}

export default UserRepository;
