import UserRepository from './UserRepository';
import CompanyRepository from './CompanyRepository';

export {
    UserRepository,
    CompanyRepository
};
