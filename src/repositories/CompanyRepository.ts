import { ObjectId, type Collection } from 'mongodb';
import { BaseCompany, UpdateCompanyInput } from '../types';

class CompanyRepository {
    db: Collection<BaseCompany>;
    constructor({ db }) {
        this.db = db;
    }

    async findOne(companyId: string) {
        const userLookup = {
            from: 'user',
            localField: 'users',
            foreignField: '_id',
            as: 'users'
        };
        const findById = [
            { $match: { _id: new ObjectId(companyId) } },
            { $lookup: userLookup }
        ];
        const [company] = await this.db.aggregate(findById).toArray();
        return company;
    }

    async createOne(company: BaseCompany) {
        const now = new Date();
        const defaultAttributes = {
            users: [],
            createdAt: now,
            updatedAt: now
        };
        Object.assign(company, defaultAttributes);
        return this.db.insertOne(company);
    }

    async updateOne(companyId: string, company: UpdateCompanyInput) {
        const now = new Date();
        const updateAttribute = {
            updatedAt: now
        };
        Object.assign(company, updateAttribute);
        return this.db.updateOne(
            { _id: new ObjectId(companyId) },
            { $set: company }
        );
    }

    async addUser(companyId: string, userId: string) {
        return this.db.updateOne(
            { _id: new ObjectId(companyId) },
            { $push: { users: new ObjectId(userId) } }
        );
    }

    async removeUser(companyId: string, userId: string) {
        return this.db.updateOne(
            { _id: new ObjectId(companyId) },
            { $pull: { users: new ObjectId(userId) } }
        );
    }

    async removeAllUsers(companyId: string) {
        return this.db.updateOne(
            { _id: new ObjectId(companyId) },
            { $set: { users: [] } }
        );
    }
}

export default CompanyRepository;
