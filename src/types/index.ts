import type { ObjectId, OptionalId } from 'mongodb';

export type NewDocument = {
    acknowledged: boolean,
    insertedId: ObjectId
}

export type BaseUser = {
    name: string,
    email: string,
    companies: ObjectId[]
}

export type UpdateUserInput = {
    name?: string,
    email?: string
}

export type BaseCompany = {
    name: string,
    yearOfCreation: number,
    users: ObjectId[]
}

export type UpdateCompanyInput = {
    name?: string,
    yearOfCreation?: number
}

export type UpdateEntityInput = UpdateUserInput | UpdateCompanyInput;

export type dbUser = OptionalId<User>;

export type Company = BaseCompany & {
    _id: ObjectId,
    users: BaseUser
}

export type User = BaseCompany & {
    _id: ObjectId,
    companies: BaseCompany
}
