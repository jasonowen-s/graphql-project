import { CompanyRepository, UserRepository } from '../repositories';

export type ServerContext = {
    userId: string,
    companyId: string,
    userRepository: UserRepository,
    companyRepository: CompanyRepository
}
