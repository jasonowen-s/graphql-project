import { MongoClient } from 'mongodb';

import { CompanyRepository, UserRepository } from './repositories';
import type configType from './config';
import { Company, User } from './types';

const connectDB = async (config: typeof configType) => {
    const { db: dbConfig } = config;
    const client: MongoClient = new MongoClient(dbConfig.connectionString);
    await client.connect();
    console.log('Connected successfully to server');
    const db = client.db(dbConfig.dbName);
    const userDB = db.collection<User>('user');
    const companyDB = db.collection<Company>('company');

    return {
        userDB, companyDB
    };
};

const createServices = async (config: typeof configType) => {
    const { userDB, companyDB } = await connectDB(config);
    return {
        userRepository: new UserRepository({ db: userDB }),
        companyRepository: new CompanyRepository({ db: companyDB })
    };
};

export default createServices;
