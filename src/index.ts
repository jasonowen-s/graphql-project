import { ApolloServer, BaseContext } from '@apollo/server';
import { startStandaloneServer } from '@apollo/server/standalone';

import config from './config';
import { typeDefs,resolvers } from './graphql';
import createServices from './createServices';

const server = new ApolloServer<BaseContext>({
    typeDefs,
    resolvers
});

// TODO: add logger
const start = async () => {
    const { companyRepository, userRepository } = await createServices(config);
    const { url } = await startStandaloneServer(server, {
        context: async ({ req }) => {
            const userId = req.headers['user-id'];
            const companyId = req.headers['company-id'];
            return {
                ...(userId && { userId }),
                ...(companyId && { companyId }),
                companyRepository,
                userRepository
            };
        },
        listen: { port: 3000 }
    });
    console.log(`Server running at ${url}`);
};

start();
