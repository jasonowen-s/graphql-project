import type { Document } from 'mongodb';
import { ServerContext } from '../types/graphql';

const throwInvalidSessionError = () => { throw new Error('Invalid Session'); };

type UserCompany = {
    role: string,
    entity: Document
}

const getConfig = (context: ServerContext) => {
    const { userId, companyId, userRepository, companyRepository } = context;
    const configs = {
        company: {
            repository: companyRepository,
            id: companyId
        },
        user: {
            repository: userRepository,
            id: userId
        }
    };
    const role = userId ? 'user' : 'company';
    const config = configs[role];
    return { ...config, role };
};

export const authenticateUser = async (context: ServerContext): Promise<Document> => {
    const { userId } = context;
    if(!userId) throwInvalidSessionError();
    const user = await context.userRepository.findOne(userId);
    if(!user) throwInvalidSessionError();
    return user;
};

// TODO: ideally we can use generated id token on login
// then we can just use jwt.verify here and use role attribute
export const authenticateEntity = async (context: ServerContext): Promise<UserCompany> => {
    const { id, repository, role } = getConfig(context);
    if(!id) throwInvalidSessionError();
    const entity = await repository.findOne(id);
    if(!entity) throwInvalidSessionError();

    return { entity, role };
};

export const authenticateCompany = async (context: ServerContext): Promise<Document> => {
    const { companyId } = context;
    if(!companyId) throwInvalidSessionError();
    const company = await context.companyRepository.findOne(companyId);
    if(!company) throwInvalidSessionError();
    return company;
};
